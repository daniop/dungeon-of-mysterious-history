import pygame, os, random
from pygame.locals import *
from funkcje import *
from color import *
def check_komenda(text,gracz,mapa,enemy,all_enemy,game_info,text_output,y_output):
        # fucnkja odpowiadajaca za sprawdzenie komendy wpisanej przez gracza i wykonanie polecenia ktore zostalo uzyte.
        # pierwsze cztery komendy sloza za ruch gracza, oraz sprawdzaja czy trwa walka
        # ostatnia komenda odpowiada za zaatakowanie przeciwnika, komenda ta wykona się tylko podacz trwania walki,
        # obrazenia jakie przeciwnik otrzyma są przedstawione za pomoca statystyki "dmg" gracza oraz pomniejszone o obrone przeciwnika
        # szansa na trafienie jest opisana statysyko "dodge" przeciwnika powiekszona o 5
        if text == '>dol':
                gracz['y'] = gracz['y'] + 1
                x = collision(gracz,mapa)
                if x==0:
                        gracz['y'] = gracz['y'] - 1
                check_fight(gracz,all_enemy,enemy,game_info)
                fight_state(gracz,enemy,game_info,text_output,y_output)
        elif text == '>gora':
                gracz['y'] = gracz['y'] - 1
                x = collision(gracz,mapa)
                if x==0:
                        gracz['y'] = gracz['y'] + 1
                check_fight(gracz,all_enemy,enemy,game_info)
                print(game_info['fight'])
                print('enemy ',enemy)
                if game_info['fight'] == 'True':
                        fight_state(gracz,enemy,game_info,text_output,y_output)
        elif text == '>lewo':
                gracz['x'] = gracz['x'] - 1
                x = collision(gracz,mapa)
                if x==0:
                        gracz['x'] = gracz['x'] + 1
                check_fight(gracz,all_enemy,enemy,game_info)
                fight_state(gracz,enemy,game_info,text_output,y_output)
        elif text == '>prawo':
                gracz['x'] = gracz['x'] + 1
                x = collision(gracz,mapa)
                if x==0:
                        gracz['x'] = gracz['x'] - 1
                check_fight(gracz,all_enemy,enemy,game_info)
                fight_state(gracz,enemy,game_info,text_output,y_output) 
        elif text == '>atak':
                if game_info['fight'] == 'True':
                        if gracz['x'] == (enemy['x'] + 1) or gracz['x'] == (enemy ['x'] - 1):
                                if gracz['y'] == (enemy['y'] + 1) or gracz['y'] == (enemy ['y'] - 1):
                                        dodge = 0
                                        dodge = random.randint(1,100)
                                        if dodge  > 5 + enemy['dodge']:
                                                dmg_given = (gracz['dmg'] - enemy['def'])
                                                enemy['hp'] = enemy['hp'] - dmg_given
                                                text_out_list = ("zadajesz przeciwnikowi",dmg_given,"obrazen")
                                                text_out_str = ''.join(map(str,text_out_list))
                                                out_put(text_out_str,text_output,y_output)
                                        else:
                                                out_put("przeciwnik uniknal twojego ataku",text_output,y_output)
                        fight_state(gracz,enemy,game_info,text,y_output)
def collision(gracz,mapa):
    # funckja sprawdza czy wystepuje kolizja gracza z sciana mapy
    x = 1
    for n in mapa:
        if int(gracz['x']) == int(n['x']) and int(gracz['y']) == int(n['y']) and int(n['object']) == 0:
                x = 0
                return(x)
    return(x)