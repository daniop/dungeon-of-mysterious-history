import pygame, os, json
from pygame.locals import *
from color import *
import random
def text_object(text, font,color):
        # funckja renderujaca powierzchnie dla funckji message
        textSurface = font.render(text, True, color)
        return textSurface, textSurface.get_rect()

def message(gameDisplay,x,y,text,color):
        # fucnkja  slozaca do wyswietlania dowolnej wiadomosci na ekranie gry
        Textfont = pygame.font.Font('freesansbold.ttf',20)
        TextSurf, TextRect = text_object(text, Textfont, color)
        TextRect = (x,y)
        gameDisplay.blit(TextSurf, TextRect)

def pozycja_gracza_message(gameDisplay,gracz):
        # funckja wyswietla pozycje gracza
        x = gracz['x']
        y = gracz['y']
        z = ('gracz','jest','na','koordynatach',x,y)
        k = ' '.join(map(str,z))
        Textfont = pygame.font.Font('freesansbold.ttf',12)
        TextSurf, TextRect = text_object(k, Textfont, white)
        TextRect = (2,200)
        gameDisplay.blit(TextSurf, TextRect)

def getDataFromJSON(path):
        # funckja odpowiedzialna za szczytywanie plikow typu json
	if os.path.isfile(path):
		with open(path) as jsonFile:
		  return json.loads(jsonFile.read())
	else:
		print("Failed to fetch data from {}".format(path))
		return None

def out_put(komenda,text,y_output):
        # funckja odpowiedzialna za rozszerzenie sie tabeli stringów które, są wyświetlane jako konsola gry
        n = len(text)
        if n == 0:
                text.append(komenda)
                x = len(text)
                y_output.append(y_output[x-1]-22)
                return (text)
        else:
                text.insert(0,komenda)
                x = len(text)
                y_output.append(y_output[x-1]-22)
                return(text)
def check_fight(gracz,all_enemy,enemy,game_info):
        # funkcja sparawdza czy trwa walka jesli tak to nic sie nie dzieje a jesli walka nie trwa to funcnkca losuje numer i przechodzi dalej do funckji spawn.
        if game_info['fight'] == 'False':
                roll = random.randint(1,100)
                game_info['enemy_spawn'] = 'True'
                if game_info['enemy_spawn'] == 'True':
                        print("roll= ",roll)
                        spawn(roll,gracz,all_enemy,enemy,game_info)
                        game_info['fight'] = 'True'
                        
        else:
                print('nie ma szans')
def fight_state(gracz,enemy,game_info,text,y_output):
        # jesli trwa walka to przeciwnik wykonuje swoj ruch, na poczatku sprawdzane jest czy przeciwnik znajduje się obok gracza. Jesli znajduje sie to przeciwnik zadaje graczowi
        # obrazenia w wysokosci statystyki potwora o nazwie "dmg" pomniejszonych o obrone gracza. Jesli przeciwnik nie znajduje się obok gracza to przeciwnik wykonuje ruch w kierunku gracza
        # pierw po osi x a pozniej po osi y.
        if game_info['fight'] == 'True':
                
                if enemy['x'] == (gracz['x'] + 1) or enemy['x'] == (gracz['x'] - 1):
                        if enemy['y'] == (gracz['y'] + 1) or enemy['y'] == (gracz['y'] - 1):
                                dodge = 0
                                dodge = random.randint(1,100)
                                if dodge  > 5 + gracz['dex']:
                                        dmg_given=(enemy['dmg'] - gracz['def'])
                                        gracz['hp'] = gracz['hp'] - dmg_given
                                        text_out_list = ("przeciwnik zadał ci",dmg_given,"obrazen")
                                        text_out_str = ''.join(map(str,text_out_list))
                                        out_put(text_out_str,text,y_output)
                elif enemy['x'] < (gracz['x']-1):
                        enemy['x'] += 1
                elif enemy['x'] > (gracz['x']+1):
                        enemy['x'] -= 1
                elif enemy['y'] < (gracz['y']-1):
                        enemy['y'] += 1
                elif enemy['y'] > (gracz['y']+1):
                        enemy['y'] -= 1
def spawn(roll,gracz,all_enemy,enemy,game_info):
        # funckja odpowiada za pojawianie sie potworow. Szansa na ich pojawienie przy kazdym ruchu wynosci 20, jest za ta szanse odpowidzalny prierwszy warunek. Nastepnie z
        # w wszystkich przeciwnikow sa wybierani tylko ci ktorzy maja taki sam poziom jak gracz. Z listy potworow o tym samym poziomie jest losowanyjeden a nastpenie jest
        # losowana pozycja potowra na obrzeżach kwadratu dookola gracza.
        if roll < 80:
                t = 0
                enemy_lvl = []
                for n in all_enemy:
                        if n['lvl'] == gracz['lvl']:
                                enemy_lvl.append(n)
                                t += 1
                random_enemy_roll = random.randint(0,t-1)
                print("roll enemy = ",random_enemy_roll)
                print('t= ',t)
                enemy = enemy_lvl[random_enemy_roll]
                print('enemy ',enemy)
                sapwn_pozition = random.randint(1,24)
                print('spawn poziton ',sapwn_pozition)
                if sapwn_pozition == 1:
                        enemy['x']=gracz['x'] - 3
                        enemy['y']=gracz['y'] - 3
                elif sapwn_pozition == 2:
                        enemy['x']=gracz['x'] - 3
                        enemy['y']=gracz['y'] - 2
                elif sapwn_pozition == 3:
                        enemy['x']=gracz['x'] - 3
                        enemy['y']=gracz['y'] - 1
                elif sapwn_pozition == 4:
                        enemy['x']=gracz['x'] - 3
                        enemy['y']=gracz['y']
                elif sapwn_pozition == 5:
                        enemy['x']=gracz['x'] - 3
                        enemy['y']=gracz['y'] + 1
                elif sapwn_pozition == 6:
                        enemy['x']=gracz['x'] - 3
                        enemy['y']=gracz['y'] + 2
                elif sapwn_pozition == 7:
                        enemy['x']=gracz['x'] - 3
                        enemy['y']=gracz['y'] + 3
                elif sapwn_pozition == 8:
                        enemy['x']=gracz['x'] - 2
                        enemy['y']=gracz['y'] + 3
                elif sapwn_pozition == 9:
                        enemy['x']=gracz['x'] - 1
                        enemy['y']=gracz['y'] + 3
                elif sapwn_pozition == 10:
                        enemy['x']=gracz['x']
                        enemy['y']=gracz['y'] + 3
                elif sapwn_pozition == 11:
                        enemy['x']=gracz['x'] + 1
                        enemy['y']=gracz['y'] + 3
                elif sapwn_pozition == 12:
                        enemy['x']=gracz['x'] + 2
                        enemy['y']=gracz['y'] + 3
                elif sapwn_pozition == 13:
                        enemy['x']=gracz['x'] + 3
                        enemy['y']=gracz['y'] + 3
                elif sapwn_pozition == 14:
                        enemy['x']=gracz['x'] + 3
                        enemy['y']=gracz['y'] + 2
                elif sapwn_pozition == 15:
                        enemy['x']=gracz['x'] + 3
                        enemy['y']=gracz['y'] + 1
                elif sapwn_pozition == 16:
                        enemy['x']=gracz['x'] + 3
                        enemy['y']=gracz['y']
                elif sapwn_pozition == 17:
                        enemy['x']=gracz['x'] + 3
                        enemy['y']=gracz['y'] - 1
                elif sapwn_pozition == 18:
                        enemy['x']=gracz['x'] + 3
                        enemy['y']=gracz['y'] - 2
                elif sapwn_pozition == 19:
                        enemy['x']=gracz['x'] + 3
                        enemy['y']=gracz['y'] - 3
                elif sapwn_pozition == 20:
                        enemy['x']=gracz['x'] + 2
                        enemy['y']=gracz['y'] - 3
                elif sapwn_pozition == 21:
                        enemy['x']=gracz['x'] + 1
                        enemy['y']=gracz['y'] - 3
                elif sapwn_pozition == 22:
                        enemy['x']=gracz['x']
                        enemy['y']=gracz['y'] - 3
                elif sapwn_pozition == 23:
                        enemy['x']=gracz['x'] - 1
                        enemy['y']=gracz['y'] - 3
                elif sapwn_pozition == 24:       
                        enemy['x']=gracz['x'] - 2
                        enemy['y']=gracz['y'] - 3
        else:
                game_info['enemy_spawn'] = 'False'
                game_info['fight'] = 'False'
                print("haha")