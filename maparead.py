import pygame, os
from pygame.locals import *
from maparead import *
from funkcje import *
from color import *

def mapa_load():
    #fucnkja slozy do wczytania mapy z pliku mapa.txt, 0 oznaczaja sciany, a 1 oznacza pole gry
    mapa = []
    struktury = []
    l = 0
    k = 0
    with open("mapa.txt") as f:
    
        struktury = f.read().split(" ")
        for n in range(len(struktury)):
            if struktury[n]=='0\n0':
                struktury[n] = 0
                mapa.append({'x':k,'y':l,'object':struktury[n]})
                l = l + 1
                k = 0   
            else:
                mapa.append({'x':k,'y':l,'object':struktury[n]})
                k = k + 1
    return mapa