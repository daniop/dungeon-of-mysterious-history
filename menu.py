import pygame, os
from pygame.locals import *
from maparead import *
from funkcje import *
from color import *
def loadimg(name):
        #funckja słóżąca do wczytywania grafik
        img = pygame.image.load(os.path.join("IMG",name))
        return img

Nowagra = loadimg("Nowagra.jpg")
Nowagra1 = loadimg('Nowagra1.jpg')
wczytaj = loadimg('wczytaj.jpg')
wczytaj1 = loadimg('wczytaj 1.jpg')
wyjscie = loadimg('wyjście.jpg')
wyjscie1 = loadimg('wyjście 1.jpg')

def button(gameDisplay,x,y,w,h,img1,img2,action=None):
    # funckja ktorej zadaniem jest stowrzenie, z grafik, interaktywnych przyciskow
    # x,y wspołrzedne obrazka, w - szerokosc obrazka, h - wysokosc obrazka
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    print(click)
    if x+w > mouse[0] > x and y+h > mouse[1] > y:
        gameDisplay.blit(img2,(x,y))

        if click[0] == 1 and action != None:
            action()         
    else:
        gameDisplay.blit(img1,(x,y))

def wczytaj_gre():

    gameExit = False
 
    while not gameExit:
 
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        gameDisplay.fill(black)            
        gameDisplay.blit(wczytaj,(300,300))

        pygame.display.update()
        clock.tick(15)