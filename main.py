#Copyright (C) <2018>  <Daniel Fijołek>

    #This program is free software: you can redistribute it and/or modify
    #it under the terms of the GNU General Public License as published by
    #the Free Software Foundation, either version 3 of the License, or
    #any later version.

    #This program is distributed in the hope that it will be useful,
    #but WITHOUT ANY WARRANTY; without even the implied warranty of
    #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #GNU General Public License for more details.

    #You should have received a copy of the GNU General Public License
    #along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pygame, os
import time
import random

from pygame.locals import *
from maparead import *
from ruch import *
from menu import *
from funkcje import *
from color import *
pygame.init()
#-------------------------------------------------------------
# fragment ten odpowiada za inicjacje danych poczatkowych gry oraz okna
display_width = 800
display_height = 600
mapa = mapa_load()
y_output = []

y_output.append(340)
all_enemy = getDataFromJSON("enemy.json")
enemy=  {}
all_eq = getDataFromJSON("eq.json")
game_info = {
        'enemy_spawn' : 'False',
        'fight': 'False'
}
gameDisplay = pygame.display.set_mode((display_width,display_height))
clock = pygame.time.Clock()
pygame.display.set_caption('Dungeon of misterious history')
#-----------------------------------------------------------


def game_menu():
    # menu gry
    # wyswietlone sa trzy przyciski interaktywne
    menu = True

    while menu:
        for event in pygame.event.get():
            print(event)
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
                
        gameDisplay.fill(black)
        
        button(gameDisplay,300,200,200,80,Nowagra,Nowagra1,game_loop)
        button(gameDisplay,300,300,200,80,wczytaj,wczytaj1,wczytaj_gre)
        button(gameDisplay,300,400,200,80,wyjscie,wyjscie1,quit)

        pygame.display.update()
        clock.tick(15)
def game_loop():

    game = True
    active = False
    text_output = []
    
    komenda = '>'
    font = pygame.font.Font(None,32)
    color = color_inactive
    
    
    print(mapa)
    gracz = {
                "x": 2,
                "y": 2,
                "hp": 10,
                "def": 0,
                "dmg": 1,
                "str": 5,
                "dex": 5,
                "int": 5,
                "lvl": 1,
                "exp": 0,
        }
        

    while game:
 
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        pygame.quit()
                        quit()
                elif event.type == pygame.KEYDOWN:
                        if event.key == K_ESCAPE:
                                pygame.quit()
                                quit()
                #----------------------------------------
                # fragment kodu odpowiedzialny za stworzenie paska wprowadzenia tekstu, ktrory jest przemieniany na komendy
                input_box = pygame.Rect(0,364,800,34)
                if event.type == pygame.MOUSEBUTTONDOWN:
                        if input_box.collidepoint(event.pos):
                                active = not active
                        else:
                                active = False
                        color = color_active if active else color_inactive
                if event.type == pygame.KEYDOWN:
                        if active:
                                if event.key == pygame.K_RETURN:
                                        text_output = out_put(komenda,text_output,y_output)
                                        check_komenda(komenda,gracz,mapa,enemy,all_enemy,game_info,text_output,y_output)

                                        komenda = '>'

                                elif event.key == pygame.K_BACKSPACE:
                                        komenda = komenda[:-1]
                                else:
                                        komenda += event.unicode
                
                gameDisplay.fill(black)
                gameDisplay.fill((30, 30, 30))
                txt_surface = font.render(komenda, True, color)
                width = max(800, txt_surface.get_width()+10)
                input_box.w = width
                gameDisplay.blit(txt_surface, (input_box.x+5, input_box.y+5))
                pygame.draw.rect(gameDisplay, color, input_box, 2)
                #------------------------------------------------
                # wyswietlanie się tesktu w oknie gry
                for n in range(len(text_output)):
                        message(gameDisplay,0,y_output[n],text_output[n],red)
                #------------------------------------------------
                
                pozycja_gracza_message(gameDisplay,gracz)
                if game_info['fight'] == 'True':
                        message(gameDisplay,400,2,"trwa walka",red)
        

                pygame.display.update()
                clock.tick(15)



game_menu()
wczytaj_gre()
game_loop()
pygame.quit()
quit()

